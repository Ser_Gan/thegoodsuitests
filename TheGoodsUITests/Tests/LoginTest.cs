﻿using System;
using NUnit.Framework;
using Xamarin.UITest;


namespace TheGoodsUITests
{
	public class LoginTest : BaseTest
	{


		public LoginTest (Platform platform) {
			base.platform = platform;
		}


		public void setup (){
			//AppInitializer.StartAppClean (platform);
		}

		//[Test]
		public void test () {

			app.Repl();

	}

		[Test]
		public void CorrectLogin (){
			WelcomeScreen welcomeScreen = new WelcomeScreen (platform);
			welcomeScreen
 				.OpenSignIn ()
				.SignIn (userName, userPassword);

		}

		[Test]
		public void TestInvalidEmail () {
			string errorMessage = "Entered email is invalid";
			WelcomeScreen welcomeScreen = new WelcomeScreen (platform);
			welcomeScreen
				.OpenSignIn ()
				.SignInWithWrongCredantials ("invalidEmail", userPassword);
			Assert.IsTrue (welcomeScreen.isErrorMessageAppeared (errorMessage), "INVALID EMAIL error message wasn't shown");				
		}


		//[Test]
		public void TestIncorrectEmail () {
			string errorMessage = "Entered email is not registered";
			WelcomeScreen welcomeScreen = new WelcomeScreen (platform);
			welcomeScreen
				.OpenSignIn ()
				.SignInWithWrongCredantials ("notRegisteredEmail@mail.com", userPassword);
			Assert.IsTrue (welcomeScreen.isErrorMessageAppeared (errorMessage), "INCORRECT EMAIL error message wasn't shown");				
		}


		//[Test]
		public void TestIncorrectPassword () {
			string errorMessage = "Entered password is incorrect";
			WelcomeScreen welcomeScreen = new WelcomeScreen (platform);
			welcomeScreen
				.OpenSignIn ()
				.SignInWithWrongCredantials (userName, "1234567890qwer");
			Assert.IsTrue (welcomeScreen.isErrorMessageAppeared (errorMessage), "INCORRECT PASSWORD error message wasn't shown");				
		}


		//[Test]
		public void TestShortPassword () {
			string errorMessage = "Password must be at least 8 characters";
			WelcomeScreen welcomeScreen = new WelcomeScreen (platform);
			welcomeScreen
				.OpenSignIn ()
				.SignInWithWrongCredantials (userName, "123");
			Assert.IsTrue (welcomeScreen.isErrorMessageAppeared (errorMessage), "INVALID PASSWORD error message wasn't shown");				
		}






	}
}
