﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace TheGoodsUITests
{
	public class AbstractScreen
	{
		public IApp app = BaseTest.app;




		//Verify if element displays on screen
		public bool IsElementAppeared (int seconds, Func<AppQuery, AppQuery> element) {
			try {
				app.WaitForElement (element, "timeOut for waiting element",TimeSpan.FromSeconds(seconds));
			} catch (TimeoutException)
			{
				app.Screenshot ("Element" + element.ToString () + " wasn't found");
				Console.WriteLine ("Element" + element.ToString() + " wasn't found");
				return false;
			}
			return true;
		}
		//Return element which is 100% displayed on the screen
		public Func<AppQuery, AppQuery> AppElement (Func<AppQuery, AppQuery> element) {
			IsElementAppeared(15,element);
			return element;
		}

		public void Tap (Func<AppQuery, AppQuery> locator){
			var elementToTap = AppElement (locator);
			app.Tap (elementToTap);
		}

		public void EnterText (Func<AppQuery, AppQuery> locator, string text) {

			var elementToEnter = AppElement (locator);
			app.ClearText (elementToEnter);
			app.EnterText (elementToEnter, text);
		}

		public void ClearField (Func<AppQuery, AppQuery> locator) {

			var elementToClear = AppElement (locator);
			app.ClearText (elementToClear);
		}

		public void TapBackButton (){
			app.Back ();
		}

		public void SwipeToRight () {

			app.SwipeRight();
		}

		public void SwipeToLeft () {

			app.SwipeLeft ();
		}

		public void ScrollDown () {
			app.ScrollDown ();
		}

		public void ScrollUp () {
			app.ScrollUp ();
		}

		public bool IsMessageDisplayed (String messageText) {
			return IsElementAppeared (5, c => c.Marked (messageText));
		}

		public bool IsMessageDisplayed (Func<AppQuery, AppQuery> locator, String messageText) {
			String actuallyMessage = locator.ToString();
			String originallMessage = messageText;
			var button = app.Query(v => v.Button("Save"));
			if (originallMessage.Equals (actuallyMessage)) {
				return true;
			} else {
				return false;
			}
		}

		public bool isErrorMessageAppeared (string messageText) {
			Func <AppQuery, AppQuery> errorMessageElement = c => c.Class ("TextView").Marked (messageText);

			return IsElementAppeared (3,errorMessageElement);
		}


		public void ConfirmPopup () {
		}

		public void TapYES () {
			Tap (c => c.Marked ("YES")); 
		}

		public void TapNO () {
			Tap (c => c.Marked ("NO"));
		} 

		public void NavigateBack (){
			app.Back ();
		}


			
	}
}

