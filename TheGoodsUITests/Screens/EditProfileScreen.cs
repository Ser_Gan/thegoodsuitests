﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace TheGoodsUITests
{
	public class EditProfileScreen : AbstractScreen
	{
		Platform platform;
		public EditProfileScreen (Platform platform)
		{
			this.platform = platform;
			//choosePlatform ();

			if (!(IsElementAppeared(3, EDIT_PROFILE_TITLE) & IsElementAppeared (3, USERNAME_FILED)))

			{
				throw new Exception("EDIT PROFILE screen wasn't opened or some of required UI elements are absent");				
			}
		}

		private Func <AppQuery, AppQuery> EDIT_PROFILE_TITLE;
		private Func <AppQuery, AppQuery> CHANGE_PHOTO_BUTTON;
		private Func <AppQuery, AppQuery> USERNAME_FILED;
		private Func <AppQuery, AppQuery> LOCATION_FIELD;
		private Func <AppQuery, AppQuery> DESCRIPTION_FIELD;
		private Func <AppQuery, AppQuery> SAVE_BUTTON;
		private Func <AppQuery, AppQuery> DONE_BUTTON;


		public EditProfileScreen ChangeUserName (string newUserName) {
			ClearField (USERNAME_FILED);
			EnterText (USERNAME_FILED, newUserName);
			return this;
		}

		public EditProfileScreen changeLocation (string newLocation){
			ClearField (LOCATION_FIELD);
			EnterText (LOCATION_FIELD, newLocation);
			return this;
		}

		public EditProfileScreen changeDescription (string newDescription) {
			ClearField (DESCRIPTION_FIELD);
			EnterText (DESCRIPTION_FIELD, newDescription);
			return this;
		}

		public PreferencesScreen SaveChanges (){
			Tap (SAVE_BUTTON);
			return new PreferencesScreen (platform);
		
		}
				


	}
}

