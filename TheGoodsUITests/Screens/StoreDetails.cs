﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace TheGoodsUITests
{
	public class StoreDetails : AbstractScreen
	{
		Platform platform;
		public StoreDetails (Platform platform)
		{
			this.platform = platform;
			//choosePlatform ();

			if (!(IsElementAppeared(3, DIRECTIONS_BUTTON))) 

			{
				throw new Exception("News Feed screen wasn't opened or some of required UI elements are absent");				
			}
		}

		private Func <AppQuery, AppQuery> STORE_TITLE;
		private Func <AppQuery, AppQuery> NAVIGATION_MENU_BUTTON;
		private Func <AppQuery, AppQuery> STORE_DESCRIPTION;
		private Func <AppQuery, AppQuery> DIRECTIONS_BUTTON;
		private Func <AppQuery, AppQuery> FOLLOW_BUTTON;
		private Func <AppQuery, AppQuery> UNFOLLOW_BUTTON;



		//follow store
		public StoreDetails StartFollowStore () {
			Tap (FOLLOW_BUTTON);
			return this;
		}

		//TRy to Follow if app is not signed in
		public SignInRequiredScreen StartFollowStore (bool isSignedIn){
			Tap (FOLLOW_BUTTON);
			return new SignInRequiredScreen ();
		}


		// go to Maps?
		//Verify store information?
		// open post

	}

}

