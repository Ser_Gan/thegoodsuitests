﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace TheGoodsUITests
{
	public class PreferencesScreen : AbstractScreen
	{
		Platform platform;
		public PreferencesScreen (Platform platform)
		{
			this.platform = platform;
			//choosePlatform ();

			if (!(IsElementAppeared(3, MY_STORES_BUTTON) & IsElementAppeared (3, EDIT_PROFILE_BUTTON) & IsElementAppeared (3, SETTING_BUTTON))) 

			{
				throw new Exception("News Feed screen wasn't opened or some of required UI elements are absent");				
			}
		}

		private Func <AppQuery, AppQuery> MY_STORES_BUTTON;
		private Func <AppQuery, AppQuery> EDIT_PROFILE_BUTTON;
		private Func <AppQuery, AppQuery> SETTING_BUTTON;
		private Func <AppQuery, AppQuery> MENU_BUTTON;


		public MyStoresScreen OpenMyStores () {
			Tap (MY_STORES_BUTTON);
			return new MyStoresScreen (platform);
		}

		public EditProfileScreen OpenEditProfile () {
			Tap (EDIT_PROFILE_BUTTON);
			return new EditProfileScreen (platform);
		}

		public SettingsScreen OpenSettings () {
			Tap (SETTING_BUTTON);
			return new SettingsScreen ();
		}

	}
}

