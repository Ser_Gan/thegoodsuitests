﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace TheGoodsUITests
{
	public class NavigationScreen : AbstractScreen
	{
		Platform platform;
		public NavigationScreen (Platform platform)
		{
			this.platform = platform;
			//choosePlatform ();

			if (!(IsElementAppeared(3, TERMS_CONDITIONS_BUTTON) & IsElementAppeared (3, ABOUT_BUTTON))) 

			{
				throw new Exception("News Feed screen wasn't opened or some of required UI elements are absent");				
			}
		}

		private Func <AppQuery, AppQuery> MY_FAVORITES_BUTTON;
		private Func <AppQuery, AppQuery> PREFERENCES_BUTTON;
		private Func <AppQuery, AppQuery> FOLLOW_STORES_BUTTON;
		private Func <AppQuery, AppQuery> ABOUT_BUTTON;
		private Func <AppQuery, AppQuery> TERMS_CONDITIONS_BUTTON;
		private Func <AppQuery, AppQuery> REPORT_PROBLEM_BUTTON;
		private Func <AppQuery, AppQuery> SIGN_OUT_BUTTON;
		private Func <AppQuery, AppQuery> SIGN_UP_BUTTON;
		private Func <AppQuery, AppQuery> NAVIGATION_MENU_BUTTON;


		//GO to Favorites section
		public MyFavoritesScreen OpenMyFavorites () {
			Tap (MY_FAVORITES_BUTTON);
			return new MyFavoritesScreen (platform);			
		}

		public PreferencesScreen OpenPreferences () {
			Tap (PREFERENCES_BUTTON);
			return new PreferencesScreen (platform);
		}

		public FollowStoresScreen OpenFollowStores () {
			Tap (FOLLOW_STORES_BUTTON);
			return new FollowStoresScreen (platform);
			
		}

		public AboutScreen OpenAboutScreen () {
			Tap (ABOUT_BUTTON);
			return new AboutScreen (platform);
		}

		public TermsConditionsScreen OpenTermsConditions () {
			Tap (TERMS_CONDITIONS_BUTTON);
			return new TermsConditionsScreen ();
		}


		public ReportProblemScreen OpenReportProblemScreen (){
			Tap (REPORT_PROBLEM_BUTTON);
			return new ReportProblemScreen ();
		}

		public WelcomeScreen SignOut () {
			Tap (SIGN_OUT_BUTTON);
			//Tap YES on appeared popup
			return new WelcomeScreen (platform);
		}
	













	}
}

