﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace TheGoodsUITests
{
	public class SignUpScreen : AbstractScreen
	{
		Platform platform;
		public SignUpScreen (Platform platform)
		{
			this.platform = platform;
			//choosePlatform ();

			if (!(IsElementAppeared(3, CREATE_ACCOUNT_TEXT) & IsElementAppeared(3, CREATE_ACCOUNT_BUTTON))) 
				
				{
				throw new Exception("SIGN UP screen wasn't opened or some of required UI elements are absent");				
				}
		}

		private Func <AppQuery, AppQuery> NAVIGATION_MENU_BUTTON;
		private Func <AppQuery, AppQuery> CONNECT_WITH_FB_BUTTON = c => c.Id ("login_button").Marked ("Log in with Facebook");
		private Func <AppQuery, AppQuery> SIGNUP_TITLE = c => c.Id("toolbar_title").Marked ("SIGN UP");
		private Func <AppQuery, AppQuery> USERNAME_FIELD = c => c.Class("EditText").Id("sign_up_username");
		private Func <AppQuery, AppQuery> EMAIL_FIELD = c => c.Class ("EditText").Id("sign_up_mail");
		private Func <AppQuery, AppQuery> PASSWORD_FIELD = c => c.Class ("EditText").Id("sign_up_password");
		private Func <AppQuery, AppQuery> CONFIRM_PASSWORD_FIELD = c => c.Class ("EditText").Id("sign_up_confirmpassword");
		private Func <AppQuery, AppQuery> ZIP_CODE_FIELD = c => c.Class ("EditText").Id("sign_up_zip");
		private Func <AppQuery, AppQuery> ZIP_CODE_BUTTON = c => c.Id ("zipLocateMe");
		private Func <AppQuery, AppQuery> TELL_US_FIELD = c => c.Class ("EditText").Id("sign_up_tellus");
		private Func <AppQuery, AppQuery> CREATE_ACCOUNT_BUTTON = c => c.Button ("sign_up_btn");
		private Func <AppQuery, AppQuery> CREATE_ACCOUNT_TEXT = c => c.Class ("TextView").Marked ("Create an account. (It's easy, we promise!)");



		public NavigationScreen OpenMenu () {
			Tap (NAVIGATION_MENU_BUTTON);
			return new NavigationScreen(platform);
		}

		public void ConnectWithFB () {
			Tap (CONNECT_WITH_FB_BUTTON);

		}

		public SignUpScreen EnterUsername (String userName) {
			EnterText (USERNAME_FIELD, userName);
			return this;
		}

		public SignUpScreen EnterEmail (String userEmail) {
			EnterText (USERNAME_FIELD, userEmail);
			return this;
		}

		public SignUpScreen EnterPassword (String userPassword) {
			EnterText (PASSWORD_FIELD, userPassword);
			return this;
		}
	
		public SignUpScreen ConfirmPassword (String userPassword) {
			EnterText (CONFIRM_PASSWORD_FIELD, userPassword);
			return this;
		}

		public SignUpScreen EnterZipCode (string zipCode) {
			EnterText (ZIP_CODE_FIELD, zipCode);
			return this;
		}

		public SignUpScreen EnterAboutText (string aboutText){
			EnterText (TELL_US_FIELD, aboutText);
			return this;
		}


		public void TapToCreateAccount (){
			Tap (CREATE_ACCOUNT_BUTTON);
		}

		public NewsFeedScreen CreateAccount (string userName, string userPassword, string userEmail){
			EnterUsername (userName);
			EnterEmail (userEmail);
			EnterPassword (userPassword);
			ConfirmPassword (userPassword);
			TapToCreateAccount ();
			return new NewsFeedScreen (platform);			
		}


















	}



}

