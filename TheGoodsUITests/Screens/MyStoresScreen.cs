﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace TheGoodsUITests
{
	public class MyStoresScreen : AbstractScreen
	{
		Platform platform;
		public MyStoresScreen (Platform platform)
		{
			this.platform = platform;
			//choosePlatform ();

			if (!(IsElementAppeared(3, MY_STORES_TITLE))) 

			{
				throw new Exception("News Feed screen wasn't opened or some of required UI elements are absent");				
			}
		}

		private Func <AppQuery, AppQuery> MY_STORES_TITLE;
		private Func <AppQuery, AppQuery> BACK_BUTTON;

		public bool IsStorePresentInList (string storeName) {
			
				Func <AppQuery, AppQuery> storeLocator = c => c.Marked (storeName);

			if (IsElementAppeared (2, storeLocator)) {
				return true;
			} else {
				ScrollDown ();
				ScrollDown ();
				return IsElementAppeared (2, storeLocator);
			}
				
		}

		public PreferencesScreen BackToPreferences () {
			Tap (BACK_BUTTON);
			return new PreferencesScreen (platform);
		}







	}
}


