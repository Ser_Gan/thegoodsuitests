﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace TheGoodsUITests
{
	public class FiltersScreen : AbstractScreen
	{
		Platform platform;
		public FiltersScreen (Platform platform)
		{
			this.platform = platform;
			//choosePlatform ();

			if (!(IsElementAppeared(3, SELECT_FILTERS_TITLE) & IsElementAppeared (3, DONE_BUTTON))) 

			{
				throw new Exception("Filters screen wasn't opened or some of required UI elements are absent");				
			}
		}

		private Func <AppQuery, AppQuery> SELECT_FILTERS_TITLE;
		private Func <AppQuery, AppQuery> DONE_BUTTON;


		public MyFavoritesScreen TapDone (){
			Tap (DONE_BUTTON);
			return new MyFavoritesScreen (platform);
		}

	}
}

