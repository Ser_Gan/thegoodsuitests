﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace TheGoodsUITests
{
	public class SettingsScreen :AbstractScreen
	{
		Platform platform;
		public SettingsScreen ()
		{
			this.platform = platform;
			//choosePlatform ();

			if (!(IsElementAppeared(3, SETTINGS_TITLE) & IsElementAppeared (3, DELETE_ACCOUNT_BUTTON))) 

			{
				throw new Exception("SETTINGS screen wasn't opened or some of required UI elements are absent");				
			}
		}

		private Func <AppQuery, AppQuery> SETTINGS_TITLE;
		private Func <AppQuery, AppQuery> CHANGE_EMAIL_BUTTON;
		private Func <AppQuery, AppQuery> EMAIL_FIELD;
		private Func <AppQuery, AppQuery> CHANGE_PASSWORD_BUTTON;
		private Func <AppQuery, AppQuery> PASSWORD_FIELD;
		private Func <AppQuery, AppQuery> DELETE_ACCOUNT_BUTTON;
		private Func <AppQuery, AppQuery> SAVE_BUTTON;
		private Func <AppQuery, AppQuery> CANT_SAVE_TITLE;
		private Func <AppQuery, AppQuery> OK_BUTTON;
		private Func <AppQuery, AppQuery> DELETE_ACCOUNT_POPUP_TEXT;
		private Func <AppQuery, AppQuery> YES_BUTTON;
		private Func <AppQuery, AppQuery> NO_BUTTON;
		private Func <AppQuery, AppQuery> ACCOUNT_DELETED_TITLE;


		//Confirmation popup locators

		private Func <AppQuery, AppQuery> CONFIRM_POPUP_TITLE;
		private Func <AppQuery, AppQuery> CONFIRM_PASSWORD_FIELD;
		private Func <AppQuery, AppQuery> SUBMIT_BUTTON;
		private Func <AppQuery, AppQuery> CANCEL_BUTTON;


		public void TapToChangeButton () {
			Tap (CHANGE_EMAIL_BUTTON);
			VerifyConfirmationPopup ();
		}
			
			

		//Enter original pass and verify that correct screen appears
		public SettingsScreen confirmOriginalPassword (String originalPass) {
			EnterText (CONFIRM_PASSWORD_FIELD, originalPass);
			Tap (SUBMIT_BUTTON);
			//verify that CHANGE button is absent
			if (IsElementAppeared(3, CHANGE_EMAIL_BUTTON)) 
			{
				throw new Exception("ERROR! See screenshot");				
			}	
			return this;
		}

		public SettingsScreen CancelConfirmation () {
			Tap (CANCEL_BUTTON);
			if (!(IsElementAppeared(3, CHANGE_EMAIL_BUTTON))) 
			{
				throw new Exception("ERROR! See screenshot");				
			}	
			return this;
		}


		private void VerifyConfirmationPopup () {
			if (!(IsElementAppeared(3, CONFIRM_POPUP_TITLE))) 
			{
				throw new Exception("CONFIRM popup wasn't opened or some of required UI elements are absent");				
			}			
		}

		public SettingsScreen enterNewEmail (string newEmail) {
			ClearField (EMAIL_FIELD);
			EnterText (EMAIL_FIELD,newEmail);
			return this;
		}

		public SettingsScreen enterNewPassword (string newPass) {
			ClearField (PASSWORD_FIELD);
			EnterText (PASSWORD_FIELD, newPass);
			return this;
		}


		public PreferencesScreen TapToSave () {
			Tap (SAVE_BUTTON);
			return new PreferencesScreen (platform);
		}

		public SettingsScreen TapToSave (bool isNewEmailValid) {
			Tap (SAVE_BUTTON);
			app.WaitForElement (CANT_SAVE_TITLE, "CANT SAVE popup wasn't displayed", TimeSpan.FromSeconds (2));
			Tap (OK_BUTTON);
			return this;
		}

		public void TapToDeleteAccount () {
			Tap (DELETE_ACCOUNT_BUTTON);
		}

		public bool isConfirmationDeleteDisplays (){
			return IsElementAppeared (2,DELETE_ACCOUNT_POPUP_TEXT);
		}

		public SettingsScreen TapNoDeleteAccount () {
			Tap (NO_BUTTON);
			return this;
		}

		public WelcomeScreen TapYesButton () {
			Tap (YES_BUTTON);
			app.WaitForElement (ACCOUNT_DELETED_TITLE, "timeOut for waiting element", TimeSpan.FromSeconds (2));
			Tap (OK_BUTTON);
			return new WelcomeScreen (platform);
		}


	}
}

