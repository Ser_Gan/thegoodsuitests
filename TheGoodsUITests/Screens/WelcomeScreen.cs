﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace TheGoodsUITests
{
	public class WelcomeScreen : AbstractScreen
	{
		Platform platform;
		public WelcomeScreen (Platform platform)
		{
			this.platform = platform;
			//choosePlatform ();

			if (!(IsElementAppeared(10, GET_STARTED_BUTTON) & IsElementAppeared(5, SIGNIN_BUTTON))) 

			{
				throw new Exception("WELCOME screen wasn't opened or some of required UI elements are absent");				
			}
		}

		private Func <AppQuery, AppQuery> GET_STARTED_BUTTON = c => c.Button().Id("button1");
		private Func <AppQuery, AppQuery> SIGNIN_BUTTON = c => c.Class("TextView").Marked ("Sign In");
		private Func <AppQuery, AppQuery> SIGNUP_BUTTON = c => c.Button().Marked("Sign Up");

		public EnterZipScreen TapGetStarted () {
			Tap (GET_STARTED_BUTTON);
			return new EnterZipScreen (platform);
		}

		public SignInScreen OpenSignIn () {
			Tap (SIGNIN_BUTTON);
			return new SignInScreen (platform);
		}

		public SignUpScreen OpenSignUp () {
			Tap (SIGNUP_BUTTON);
			return new SignUpScreen (platform);
		}

	}
}

