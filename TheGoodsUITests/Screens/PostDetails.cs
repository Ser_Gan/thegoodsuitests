﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace TheGoodsUITests
{
	public class PostDetails : AbstractScreen
	{
		Platform platform;
		public PostDetails (Platform platform)
		{
			this.platform = platform;
			//choosePlatform ();

			if (!(IsElementAppeared(3, POST_TITLE))) 

			{
				throw new Exception("News Feed screen wasn't opened or some of required UI elements are absent");				
			}
		}

		private Func <AppQuery, AppQuery> POST_IMAGE = c => c.Id ("goodsImage");
		private Func <AppQuery, AppQuery> POST_TIME = c => c.Id ("time");
		private Func <AppQuery, AppQuery> POST_TITLE = c => c.Class ("TextView").Id ("toolbar_title");
		private Func <AppQuery, AppQuery> POST_PRICE;
		private Func <AppQuery, AppQuery> POSTED_BY_NAME = c => c.Marked ("Posted by ").Sibling (0);
		private Func <AppQuery, AppQuery> FAVORITES_ICON;
		private Func <AppQuery, AppQuery> COMMENTS_ICON;
		private Func <AppQuery, AppQuery> SHARE_BUTTON;
		private Func <AppQuery, AppQuery> ADD_COMMENT_FIELD;
		private Func <AppQuery, AppQuery> ADD_COMMENT_BUTTON;
		private Func <AppQuery, AppQuery> POST_COMMENT;
		private Func <AppQuery, AppQuery> NAVIGATION_MENU_BUTTON;
		private Func <AppQuery, AppQuery> STORE_TITLE;



		public NavigationScreen OpenMenu () {
			Tap (NAVIGATION_MENU_BUTTON);
			return new NavigationScreen (platform);
		}

		public StoreDetails OpenStoreDetails () {
			Tap (STORE_TITLE);
			return new StoreDetails (platform);
		}

		//Tap to Favorite icon and add item to Favorites.
		public PostDetails TapToFavorite () {
			Tap (FAVORITES_ICON);
			return this;
		}

		//Tap to Favorite icon when application is NOT signed in. So we are getting popup with propose to sign up/in
		public SignInRequiredScreen TapToFavorite (bool isSignedIn) {
			Tap (FAVORITES_ICON);
			return new SignInRequiredScreen ();			
		}

		//Tap to Comment icon to call comments field
		public PostDetails TapToComment (){
			Tap (COMMENTS_ICON);
			if (!(IsElementAppeared (2,ADD_COMMENT_FIELD) & IsElementAppeared (2,ADD_COMMENT_BUTTON))) {
				throw new Exception ("Comments field not found on screen");
			}
			return this;
		}

		//Tap to Comment icon when app is not signed in. So we are getting popup with propose to sign up/in
		public SignInRequiredScreen TapToComment (bool isSignedIn){
			Tap (COMMENTS_ICON);
			return new SignInRequiredScreen ();
		}

		//Open Share screen
		public ShareScreen OpenShareScreen () {
			Tap (SHARE_BUTTON);
			return new ShareScreen ();
		}

		//Try to open share screen when app is not signed in
		public SignInRequiredScreen OpenShareScreen (bool isSignedIn) {
			Tap (SHARE_BUTTON);
			return new SignInRequiredScreen ();
		}

		public PostDetails AddComment (string commentText){
			TapToComment ();
			EnterText (ADD_COMMENT_FIELD, commentText);
			Tap (ADD_COMMENT_BUTTON);
			return this;
		}

		//get text of comment
		//get count of comments
		//get price
		//get count of favorites






















	}
}

