﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;
namespace TheGoodsUITests
{
	public class EnterZipScreen : AbstractScreen
	{
		Platform platform;
		public EnterZipScreen (Platform platform)
		{
			this.platform = platform;
			//choosePlatform ();

			if (!(IsElementAppeared(3, ENTER_CODE_MESSAGE))) 

			{
				throw new Exception("ENTER CODE screen wasn't opened or some of required UI elements are absent");				
			}
		}

		private Func <AppQuery, AppQuery> THE_GOODS_TITLE;
		private Func <AppQuery, AppQuery> ENTER_CODE_MESSAGE = c => c.Class ("TextView").Id ("Enter your zip code to\nfind stores near you.");
		private Func <AppQuery, AppQuery> ZIP_CODE_FIELD = c => c.Class ("EditText").Id ("zipField");
		private Func <AppQuery, AppQuery> GET_LOCATION_BUTTON = c => c.Class ("ImageView").Id ("zipLocateMe");
		private Func <AppQuery, AppQuery> NEXT_BUTTON = c => c.Button("Next");


		public EnterZipScreen EnterZipCode (string zipCode) {
			EnterText (ZIP_CODE_FIELD, zipCode);
			return this;
		}

//		public ChooseStoresScreen TapNext () {
//			Tap (NEXT_BUTTON);
//			return new ChooseStoresScreen ();
//		}


	}
}

