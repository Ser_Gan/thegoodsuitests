﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace TheGoodsUITests
{
	public class MyFavoritesScreen : AbstractScreen
	{
		Platform platform;
		public MyFavoritesScreen (Platform platform)
		{
			this.platform = platform;
			//choosePlatform ();

			if (!(IsElementAppeared(3, MY_FAVORITES_TITLE) & IsElementAppeared (3, FILTER_BY_DEPT_BUTTON))) 

			{
				throw new Exception("My Favorites screen wasn't opened or some of required UI elements are absent");				
			}
		}

		private Func <AppQuery, AppQuery> MY_FAVORITES_TITLE;
		private Func <AppQuery, AppQuery> FILTER_BY_DEPT_BUTTON;
		private Func <AppQuery, AppQuery> POST_IMAGE;
		private Func <AppQuery, AppQuery> EDIT_BUTTON;
		private Func <AppQuery, AppQuery> NAVIGATION_MENU_BUTTON;
		private Func <AppQuery, AppQuery> ACCOUNT_NAME;
		private Func <AppQuery, AppQuery> CLEAR_FILTERS;


		public FiltersScreen OpenFilters () {
			Tap (FILTER_BY_DEPT_BUTTON);
			return new FiltersScreen(platform);
		}

		public bool IsAccountInfoCorrect (String accountName) {
			String actuallyName = ACCOUNT_NAME.ToString ();

			// Compare displayed name with anme which was defined while account creation
			return accountName.Equals (actuallyName);
		}

		public PostDetails OpenPost () {
			Tap (POST_IMAGE);
			return new PostDetails (platform);
		}

		public NavigationScreen OpenNavigationMenu () {
			Tap (NAVIGATION_MENU_BUTTON);
			return new NavigationScreen (platform);
		}

		public MyFavoritesScreen ClearFilters () {
			Tap (CLEAR_FILTERS);
			return this;
		}

		public EditProfileScreen OpenEditProfile () {
			Tap (EDIT_BUTTON);
			return new EditProfileScreen(platform);
		}





	 }
}

