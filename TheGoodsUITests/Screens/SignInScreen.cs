﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace TheGoodsUITests
{
	public class SignInScreen : AbstractScreen
	{
		Platform platform;
		public SignInScreen (Platform platform)
		{
			this.platform = platform;
			//choosePlatform ();

			if (!(IsElementAppeared(3, SIGNIN_TITLE) //& IsElementAppeared (3, FORGOT_PASS_LINK)
			)) 

			{
				throw new Exception("SIGNIN screen wasn't opened or some of required UI elements are absent");				
			}
		}

		private Func <AppQuery, AppQuery> SIGNIN_TITLE = c => c.Class("TextView").Marked ("Sign In");
		private Func <AppQuery, AppQuery> CONNECT_WITH_FB_BUTTON = c => c.Id ("login_button");
		private Func <AppQuery, AppQuery> EMAIL_FIELD = c => c.Class("EditText").Id("sign_in_login");
		private Func <AppQuery, AppQuery> PASSWORD_FIELD = c => c.Class("EditText").Id("sign_in_password");
		private Func <AppQuery, AppQuery> SIGNIN_BUTTON = c => c.Button().Id("sign_in_btn");
		private Func <AppQuery, AppQuery> FORGOT_PASS_LINK;


		public void ConnectWithFB () {
			Tap (CONNECT_WITH_FB_BUTTON);
		}

		public SignInScreen EnterEmail (String userEmail) {
			EnterText (EMAIL_FIELD, userEmail);
			return this;
		}

		public SignInScreen EnterPassword (String userPassword) {
			EnterText (PASSWORD_FIELD, userPassword);
			return this;
		}

		public void SubmitSignIn () {
			Tap (SIGNIN_BUTTON);
		}


		public NewsFeedScreen SignIn (string userMail, string userPassword){
			EnterEmail (userMail);
			EnterPassword (userPassword);
			SubmitSignIn ();
			return new NewsFeedScreen (platform);
		}

		public SignInScreen SignInWithWrongCredantials (string mail, string password) {
			EnterEmail (mail);
			EnterPassword (password);
			SubmitSignIn ();
			return this;
		}







	}
}

