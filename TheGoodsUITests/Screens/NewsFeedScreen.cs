﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace TheGoodsUITests
{
	public class NewsFeedScreen : AbstractScreen
	{
		Platform platform;
		public NewsFeedScreen (Platform platform)
		{
			this.platform = platform;
			//choosePlatform ();

			if (!(IsElementAppeared(10, THE_GOODS_TITLE))) 

			{
				throw new Exception("NEWS FEES screen wasn't opened or some of required UI elements are absent");				
			}
		}

		private Func <AppQuery, AppQuery> THE_GOODS_TITLE  = c => c.Class ("TextView").Marked ("Create an account. (It's easy, we promise!)") ;
		private Func <AppQuery, AppQuery> STORE_TITLE_BUTTON = c => c.Id ("storeInfo");
		private Func <AppQuery, AppQuery> NAVIGATION_MENU_BUTTON = c => c.Id ("toolbar_navigation_button");
		private Func <AppQuery, AppQuery> POST_IMAGE = c => c.Id ("goodsImage");
		private Func <AppQuery, AppQuery> POST_TIME = c => c.Id ("time");
		private Func <AppQuery, AppQuery> POST_DETAILS = c => c.Id ("showGoodArrow");


		public NavigationScreen OpenMenu () {
			Tap (NAVIGATION_MENU_BUTTON);
			return  new NavigationScreen (platform);
		}

		public StoreDetails OpenStoreDetails () {
			Tap (STORE_TITLE_BUTTON);
			return new StoreDetails (platform);
			
		}

		public PostDetails OpenPost () {
			Tap (POST_DETAILS);
			return new PostDetails (platform);
		}


		// To verify that popup after signUp displays on News Feed
		public bool IsPopupWasDisplayed () {
			return false;			
		}

	}
}

