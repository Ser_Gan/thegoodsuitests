﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace TheGoodsUITests
{
	public class FollowStoresScreen : AbstractScreen
	{
		Platform platform;
		public FollowStoresScreen (Platform platform)
		{
		}

		private Func <AppQuery, AppQuery> FOLLOW_STORES_TITLE;
		private Func <AppQuery, AppQuery> DONE_BUTTON;
		private Func <AppQuery, AppQuery> SEARCH_BY_ZIP_BUTTON;
		private Func <AppQuery, AppQuery> SEARCH_BY_STATE_BUTTON;
		private Func <AppQuery, AppQuery> SEARCH_FIELD;
		private Func <AppQuery, AppQuery> MENU_BUTTON;



		public FollowStoresScreen SearchByState (string stateName) {
			Tap (SEARCH_BY_STATE_BUTTON);
			Func <AppQuery, AppQuery> stateLocator = c => c.Marked (stateName);
			Tap (stateLocator);
			return this;

			// TODO: Verify list of stores
		}

		public NewsFeedScreen TapDone () {
			Tap (DONE_BUTTON);
			return new NewsFeedScreen (platform);
		}

		public FollowStoresScreen SearchByZip (string zipCode) {
			ClearField (SEARCH_FIELD);
			Tap (SEARCH_FIELD);
			EnterText (SEARCH_FIELD, zipCode);
			Tap (SEARCH_BY_ZIP_BUTTON);
			return new FollowStoresScreen (platform);
			//TODO: Verify list of stores
		}

		public NavigationScreen OpenNavigationMenu () {
			Tap (MENU_BUTTON);
			return new NavigationScreen (platform);
		}

	}
}

