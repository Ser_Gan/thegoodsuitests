﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace TheGoodsUITests
{
	public class AboutScreen : AbstractScreen
	{
		Platform platform;
		public AboutScreen (Platform platform)
		{
			this.platform = platform;
			//choosePlatform ();

			if (!(IsElementAppeared(3, ABOUT_CONTENT_TEXT) & IsElementAppeared (3,ABOUT_TITLE)))

			{
				throw new Exception("ABOUT screen wasn't opened or some of required UI elements are absent");				
			}
		}

		private Func <AppQuery, AppQuery> ABOUT_CONTENT_TEXT;
		private Func <AppQuery, AppQuery> ABOUT_TITLE;
		private Func <AppQuery, AppQuery> MENU_BUTTON;

		public NavigationScreen OpenMenu (){
			Tap (MENU_BUTTON);
			return new NavigationScreen (platform);
		}
	}


}

