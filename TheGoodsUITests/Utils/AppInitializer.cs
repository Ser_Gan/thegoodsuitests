﻿using System;
using Xamarin.UITest;

namespace TheGoodsUITests
{
	public class AppInitializer
	{
		public static IApp StartAppClean (Platform platform) {
			if (platform == Platform.iOS) 
			{

				return BaseTest.app = ConfigureApp
					.iOS
					.AppBundle (Properties.iOSBundle)
					.EnableLocalScreenshots()
					.StartApp (appDataMode:Xamarin.UITest.Configuration.AppDataMode.Clear);
			} 
			else 
			{
				return BaseTest.app = ConfigureApp
					.Android
					.ApkFile (Properties.androidApk)
					.EnableLocalScreenshots()
					.StartApp (appDataMode:Xamarin.UITest.Configuration.AppDataMode.Clear);
			}
		}

		public static IApp StartApp (Platform platform) {
			if (platform == Platform.iOS) 
			{

				return BaseTest.app = ConfigureApp
					.iOS
					.AppBundle (Properties.iOSBundle)
					.EnableLocalScreenshots()
					.StartApp (appDataMode:Xamarin.UITest.Configuration.AppDataMode.DoNotClear);
			} 
			else 
			{
				return BaseTest.app = ConfigureApp
					.Android
					.ApkFile (Properties.androidApk) 
					.EnableLocalScreenshots()
					.StartApp (appDataMode:Xamarin.UITest.Configuration.AppDataMode.DoNotClear);
			}
		}

	}
}

